#!/usr/bin/env python3
import os
import subprocess
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
gi.require_version('Notify', '0.7')
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk, GdkPixbuf
from gi.repository import Gtk
from gi.repository import AppIndicator3 as AppIndicator
from gi.repository import Notify

APPINDICATOR_ID = 'ec4l_appindicator'


class Ec4LinuxServiceException(Exception):

    def __init__(self, message, error=None):
        self.message = message
        self.error = error


class ResourcesException(Exception):

    def __init__(self, message, error=None):
        self.message = message
        self.error = error

class Resources:

    LOGO = 'ec4linuxlogo.svg'

    @staticmethod
    def get_path_image(image_name):
        if os.path.exists(os.path.join(Resources.get_base_path(), 'pixmaps', image_name)):
            return os.path.join(Resources.get_base_path(), 'pixmaps', image_name)
        else:
            return None

    @staticmethod
    def get_icon():
        return 'ec4linuxindicator'

    @staticmethod
    def get_path_logo():
        if Resources.get_path_image(Resources.LOGO) is None:
            raise ResourcesException("ExtremeCooling4Linux logo is not found")
        else:
            return Resources.get_path_image(Resources.LOGO)

    @staticmethod
    def get_base_path():
        if os.path.exists(os.path.join(os.path.dirname(__file__), 'resources')):
            path = os.path.join(os.path.dirname(__file__), 'resources', 'usr', 'share')
            return path
        else:
            path = os.path.join(os.sep, 'usr', 'share')
            return path


class Ec4LinuxService:


    def start_ec(self):
        try:
            subprocess.call(["pkexec", "ec4Linux.py", "enable"])
        except OSError as ox:
            raise Ec4LinuxServiceException("pkexec is not found", ox)
        except subprocess.CalledProcessError as e:
            print(e)
            raise Ec4LinuxServiceException("Error starting ExtremeCooling4Linux", e)

    def stop_ec(self):
        try:
            subprocess.call(["pkexec", "ec4Linux.py", "disable"])
        except OSError as ox:
            print(ox)
            raise Ec4LinuxServiceException("pkexec is not found", ox)
        except subprocess.CalledProcessError as e:
            print(e)
            raise Ec4LinuxServiceException("Error stopping ExtremeCooling4Linux", e)


class Ec4LinuxAppIndicator:

    def __init__(self):
        self.ec4_service = Ec4LinuxService()
        icon = Resources.get_icon()
        self.indicator = AppIndicator.Indicator.new(APPINDICATOR_ID, icon, AppIndicator.IndicatorCategory.HARDWARE)
        self.indicator.set_status(AppIndicator.IndicatorStatus.ACTIVE)
        self.menu = self.build_menu()
        self.indicator.set_menu(self.menu)
        Notify.init(APPINDICATOR_ID)
        Gtk.main()

    def build_menu(self):
        menu = Gtk.Menu()
        item_enable = Gtk.MenuItem(label='Start ExtremeCooling')
        item_enable.connect('activate', self.on_start_extreme_cooling)
        menu.append(item_enable)
        item_disable = Gtk.MenuItem(label='Stop ExtremeCooling')
        item_disable.connect('activate', self.on_stop_extreme_cooling)
        menu.append(item_disable)
        menu.append(Gtk.SeparatorMenuItem())
        item_about = Gtk.MenuItem(label="About")
        item_about.connect("activate", Ec4LinuxAppIndicator.on_show_about)
        menu.append(item_about)
        item_quit = Gtk.MenuItem('Quit')
        item_quit.connect('activate', self.on_quit)
        menu.append(item_quit)
        menu.show_all()
        return menu

    @staticmethod
    def on_show_about(widget):
        about = Gtk.AboutDialog()
        about.set_program_name("ExtremeCooling4Linux AppIndicator")
        about.set_version("0.1")
        about.set_authors(["Alberto Vicente"])
        about.set_artists(["rredesigns"])
        about.set_copyright('Alberto Vicente')
        about.set_comments("An ExtremeCooling4Linux applet")
        about.set_website("https://gitlab.com/OdinTdh/indicator-extremecooling4linux")
        about.set_website_label("https://gitlab.com/OdinTdh/indicator-extremecooling4linux")
        about.set_license_type(Gtk.License.GPL_3_0)
        try:
            about.set_logo(GdkPixbuf.Pixbuf.new_from_file_at_size(Resources.get_path_logo(), 32, 32))
        except ResourcesException as rx:
            print(rx)
        about.run()
        about.destroy()

    def on_stop_extreme_cooling(self, param):
        on_stop_notification = Notify.Notification.new("Stop", "Stopping Extreme Cooling mode", None)
        try:
            self.ec4_service.stop_ec()
        except Ec4LinuxServiceException as ex:
            on_stop_notification = Notify.Notification.new("Error", ex.message, None)
        finally:
            on_stop_notification.set_timeout(Notify.EXPIRES_DEFAULT)
            on_stop_notification.show()

    def on_start_extreme_cooling(self, param):
        on_start_notification = Notify.Notification.new("Start", "Starting Extreme Cooling mode", None)
        try:
            self.ec4_service.start_ec()
        except Ec4LinuxServiceException as ex:
            on_start_notification = Notify.Notification.new("Error", ex.message, None)
        finally:
            on_start_notification.set_timeout(Notify.EXPIRES_DEFAULT)
            on_start_notification.show()

    def on_quit(self, param):
        Notify.uninit()
        Gtk.main_quit()


if __name__ == "__main__":
    Ec4LinuxAppIndicator()
# indicator-extremecooling4linux

A desktop indicator for [ExtremeCooling4Linux](https://gitlab.com/OdinTdh/extremecooling4linux).

## Dependencies

indicator-extremecooling4linux needs the following packages installed in your system:

* python3-gi
* python3-gi-cairo
* gir1.2-gtk-3.0
* gir1.2-notify-0.7
* gir1.2-appindicator3-0.1
* extremecooling4linux

So before installing indicator-extremecooling4linux, you have to install all its dependencies.
You can use the deb package from [itch.io](https://odintdh.itch.io/extremecooling4linux)  
or the source tar.gz package from [gitlab](https://gitlab.com/OdinTdh/extremecooling4linux) to
install ExtremeCooling4Linux.


## Installation


You can install it using the deb package from
[itch.io](https://odintdh.itch.io/extremecooling4linux) or installing from source with the following command:

    sudo make install
    
To uninstall it:

    sudo make uninstall

## Arch

If you use an Arch based distribution you can install it from [AUR](https://aur.archlinux.org/packages/indicator-extremecooling4linux) with an AUR helper like yay.
   
## Usage

If you have installed indicator-extremecooling4linux it will be automatic displayed in your
panel desktop at starting. If you want to execute it directly you can do it with the command ec4linux-applet


![indicator-extremecooling4linux](indicator-ec4l_screenshot.png)

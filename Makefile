.PHONY:  install uninstall
install:
	cp -f ec4linuxindicator/ec4linux_indicator.py $(DESTDIR)/usr/bin/ec4linux-applet
	chmod 755 $(DESTDIR)/usr/bin/ec4linux-applet
	install -m 644 ec4linuxindicator/resources/usr/share/pixmaps/ec4linuxindicator.svg $(DESTDIR)/usr/share/pixmaps/
	install -m 644 ec4linuxindicator/resources/usr/share/pixmaps/ec4linuxlogo.svg $(DESTDIR)/usr/share/pixmaps/
	install -m 644 etc/xdg/autostart/ec4linux-indicator.desktop $(DESTDIR)/etc/xdg/autostart/
	install -m 644 ec4linuxindicator/resources/usr/share/pixmaps/ec4linuxindicator.svg $(DESTDIR)/usr/share/icons/hicolor/symbolic/apps/
	mv $(DESTDIR)/usr/share/icons/hicolor/symbolic/apps/ec4linuxindicator.svg $(DESTDIR)/usr/share/icons/hicolor/symbolic/apps/ec4linuxindicator-symbolic.svg
uninstall:
	rm -f $(DESTDIR)/usr/bin/ec4linux-applet
	rm -f $(DESTDIR)/usr/share/pixmaps/ec4linuxindicator.svg
	rm -f $(DESTDIR)/usr/share/pixmaps/ec4linuxlogo.svg
	rm -f $(DESTDIR)/etc/xdg/autostart/ec4linux-indicator.desktop
	rm -f $(DESTDIR)/usr/share/icons/hicolor/symbolic/apps/ec4linuxindicator-symbolic.svg
